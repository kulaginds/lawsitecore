<?php

	require_once(__DIR__.'/../../include/config.php');
	require_once(SYSTEM_DIR.'/helpers.php');
	require_once(SYSTEM_DIR.'/auth/functions.php');
    require_once(SYSTEM_DIR.'/comments/functions.php');
    require_once(SYSTEM_DIR.'/comments/helpers.php');
    require_once(ADMIN_DIR.'/lib/constants.php');
    require_once(ADMIN_DIR.'/lib/functions.php');
    require_once(ADMIN_DIR.'/lib/helpers.php');

    check_auth(ADMIN_LOGIN_URL);
    check_admin(ADMIN_NO_PERMISSIONS);

    $caption            = 'Статистика';
    $content_template   = 'stat';
    $datepicker         = true;
    $new_comments_count = get_new_comments_count();
    $comments           = array();

    if (!empty($_POST['from'])) {
        $_SESSION['from'] = $_POST['from'];
    } else if (empty($_SESSION['from'])) {
        $_SESSION['from'] = date('Y-m-d', time() - 60 * 60 * 24);
    }
    
    if (!empty($_POST['to'])) {
        $_SESSION['to'] = $_POST['to'];
    } else if (empty($_SESSION['to'])) {
        $_SESSION['to'] = date('Y-m-d');
    }

    extract(get_stat_variables());

    $comments_stat = get_comments_stat_between($_SESSION['from'], $_SESSION['to']);

    if (!empty($_GET['date']) && (FALSE !== strtotime($_GET['date']))) {
        $comments = get_comments_by_date($_GET['date']);
    }

    include(ADMIN_TEMPLATES_DIR.'/layout.tpl');

?>