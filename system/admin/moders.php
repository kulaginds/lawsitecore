<?php

	require_once(__DIR__.'/../../include/config.php');
    require_once(SYSTEM_DIR.'/functions.php');
    require_once(SYSTEM_DIR.'/img_functions.php');
	require_once(SYSTEM_DIR.'/helpers.php');
	require_once(SYSTEM_DIR.'/auth/functions.php');
    require_once(SYSTEM_DIR.'/comments/functions.php');
    require_once(SYSTEM_DIR.'/moders/functions.php');
    require_once(ADMIN_DIR.'/lib/functions.php');
    require_once(ADMIN_DIR.'/lib/constants.php');
    require_once(ADMIN_DIR.'/lib/messages.php');

    check_auth(ADMIN_LOGIN_URL);
    check_admin(ADMIN_NO_PERMISSIONS);

    $caption            = 'Модераторы';
    $content_template   = 'moders';
    $new_comments_count = get_new_comments_count();
    $moders             = get_moders();

    if (array_key_exists('delete', $_GET) && array_key_exists('moder_id', $_GET)) {
        $moder_id = (int)$_GET['moder_id'];

        if ($moder_id > 0) {
            delete_moder($moder_id);
            redirect_to(ADMIN_MODERS_URL);
        }
    }

    if (!empty($_POST['add'])) {
        $name = trim($_POST['name']);
        $pass = trim($_POST['pass']);
        $img  = null;

        $pass = add_salt_password($pass);

        if (array_key_exists(ADMIN_MODER_IMG_NAME, $_FILES)) {
            $img = upload_img($_FILES[ADMIN_MODER_IMG_NAME], ADMIN_AVATAR_DIR, ADMIN_MODERS_AVATAR_URL, ADMIN_MODER_IMG_WIDTH, ADMIN_MODER_IMG_HEIGHT);
        }

        if (!empty($_POST['url']) && is_null($img)) {
            $img = reload_img($_POST['url'], ADMIN_AVATAR_DIR, ADMIN_MODERS_AVATAR_URL, ADMIN_MODER_IMG_WIDTH, ADMIN_MODER_IMG_HEIGHT);
        }

        if (is_null($img)) {
            $img = ADMIN_MODER_DEFAULT_IMG;
        }

        if (!add_moder($name, $pass, $img)) {
            $error = $messages['add_moder_error'];
        } else {
            redirect_to(ADMIN_MODERS_URL);
            die();
        }
    }

    include(ADMIN_TEMPLATES_DIR.'/layout.tpl');

?>