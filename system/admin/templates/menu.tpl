            <? if (is_admin()): ?>
                <a href="<?=ADMIN_MODERS_URL;?>" class="font_2">Модераторы</a><br/>
            <? endif; ?>
                <a href="<?=ADMIN_CONTENT_URL;?>" class="font_2">Статьи</a><br/>
                <a href="<?=ADMIN_MODERATE_URL;?>" class="font_2">Сообщения</a>&nbsp;<span class="counter<?=print_text_param($new_comments_count, ' active');?>">(<?=$new_comments_count;?>)</span><br/>
            <? if (is_admin()): ?>
                <a href="<?=ADMIN_TEMPLATES_URL;?>" class="font_2">Шаблоны</a><br/>
                <a href="<?=ADMIN_STAT_URL;?>" class="font_2">Статистика</a><br/>
                <a href="<?=ADMIN_RATING_URL;?>" class="font_2">Рейтинг</a><br/>
                <a href="<?=ADMIN_CONTENT_DATE_URL;?>" class="font_2">Дата пересмотра статей</a><br/>
                <br/>
                <a href="<?=ADMIN_CACHE_URL;?>" class="font_2">Обновить кэш</a><br/>
                <br/>
                <a href="<?=ADMIN_UPDATE_URL;?>" class="font_2">Обновить сайт</a><br/>
            <? endif; ?>
                <a href="<?=ADMIN_QUIT_URL;?>" class="font_2">Выйти</a>