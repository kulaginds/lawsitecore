<? if ($action == 'moderate'): ?>
<h1>Редактирование комментария</h1>
<? else: ?>
<h1>Добавить новый комментарий</h1>
<? endif; ?>
<br/>
<form method="POST">
    <input type="hidden" name="content_id" value="<?=$content_id;?>" />
    <input type="hidden" name="comment_id" value="<?=$comment_id;?>" />
    <input type="hidden" name="redirect" value="<?=$redirect;?>" />
    <table class="non-full font_2" border="0" align="center" cellpadding="0" style="border-spacing: 5px 5px !important;">
<? if ($action == 'moderate'): ?>
    <tr>
        <td>Статья:</td>
        <td><a href="<?=$url;?>" target="_blank"><?=$url;?></a></td>
    </tr>
<? endif; ?>
    <tr>
        <td>Имя:</td>
        <td><input type="text" name="name" style="width: 300px;" value="<?=$name;?>" /></td>
    </tr>
    <tr>
        <td>Аватарка:</td>
        <td><input type="text" name="img" style="width: 300px;" value="<?=$img;?>" /></td>
    </tr>
    <tr>
        <td>Дата:</td>
        <td><input type="text" name="date" class="datepicker" style="width: 300px;" value="<?=$date;?>" /></td>
    </tr>
    <tr>
        <td>Отвечает:</td>
        <td>
            <? if (!is_admin()
                    && (count($moders) == 1)
                    && !in_array($moder_id, array($moders[0]['id'], -777))): ?>
            <p>Не вы</p>
            <input type="hidden" name="moder_id" value="<?=$moder_id;?>" />
            <? else: ?>
            <?=render_moders_box($moders, $moder_id);?>
            <? endif; ?>
        </td>
    </tr>
    <tr>
        <td>Вопрос:</td>
        <td><textarea name="text" cols=40 rows=10 style="width: 500px;"><?=$text;?></textarea></td>
    </tr>
    <tr>
        <td>Ответ:</td>
        <td><textarea name="answer" cols=30 rows=10 style="width: 500px;"><?=$answer;?></textarea></td>
    </tr>
     <tr>
        <td></td>
        <td><input type="submit" name="<?=$action;?>" value="Отправить" /></td>
    </tr>
    
    
    </table><br/>

</form>