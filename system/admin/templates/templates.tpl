<ul align=left>
	<li>$comments - поле со списком комментариев</li>
	<li>$comments_count - количество комментов</li>
	<li>$img - ссылка на аватар спрашивающего</li>
	<li>$name - имя спрашивающего</li>
	<li>$date - дата</li>
	<li>$text - текст вопроса</li>
	<li>$moder_img - ссылка на аватар отвечающего</li>
	<li>$moder_name - имя отвечающего</li>
	<li>$moder_comments - количество комментариев модератора</li>
	<li>$answer - текст ответа</li>
	<li>$style - Дополнительные стили блока с ответом (вставлять обязательно)</li>
</ul>
<form method="POST">
	Шаблон блока комментариев:<br/>
	<textarea name="block" cols=70 rows=10><?=$block;?></textarea>
	<br/><br/>
	Шаблон формы вопроса:<br/>
	<textarea name="comment" cols=70 rows=10><?=$comment;?></textarea>
	<br/><br/>
	Шаблон формы ответа:<br/>
	<textarea name="answer" cols=70 rows=10><?=$answer;?></textarea>
	<br/><br/>
	<input type="submit" name="save" value="Сохранить" />
</form>