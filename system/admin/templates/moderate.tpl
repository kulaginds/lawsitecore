<? if ($comments_enabled): ?>
<div class="toggle_comments">Добавление комментариев включено.<a class="cutebutton" href="<?=ADMIN_MODERATE_COMMENTS_DISABLE;?>">Отключить</a></div>
<? else: ?>
<div class="toggle_comments">Добавление комментариев отключено.<a class="cutebutton" href="<?=ADMIN_MODERATE_COMMENTS_ENABLE;?>">Включить</a></div>
<? endif; ?>
<br><br>
<?=render_comments($comments);?>