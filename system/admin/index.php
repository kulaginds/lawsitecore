<?php

	require_once(__DIR__.'/../../include/config.php');
	require_once(SYSTEM_DIR.'/helpers.php');
	require_once(SYSTEM_DIR.'/auth/functions.php');
    require_once(ADMIN_DIR.'/lib/constants.php');
    require_once(ADMIN_DIR.'/lib/functions.php');
    require_once(ADMIN_DIR.'/lib/helpers.php');

    check_auth(ADMIN_LOGIN_URL);

    if (is_admin()) {
        redirect_to(ADMIN_DEFAULT_ADMIN_PAGE);
    } else {
        redirect_to(ADMIN_DEFAULT_MODER_PAGE);
    }

?>