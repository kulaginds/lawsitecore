<?php

	require_once(__DIR__.'/../../include/config.php');
	require_once(SYSTEM_DIR.'/helpers.php');
    require_once(ADMIN_DIR.'/lib/constants.php');
	require_once(SYSTEM_DIR.'/auth/functions.php');
    require_once(SYSTEM_DIR.'/comments/functions.php');
    require_once(SYSTEM_DIR.'/date/functions.php');
    require_once(SYSTEM_DIR.'/rating/functions.php');
    require_once(SYSTEM_DIR.'/rating/helpers.php');

    check_auth(ADMIN_LOGIN_URL);
    check_admin(ADMIN_NO_PERMISSIONS);

    if (array_key_exists('id', $_GET) && array_key_exists('delete', $_GET)) {
        // удалить рейтинг, комменты, дату и сделать редирект на страницу дат статей
        $content_date = get_content_date_by_id((int)$_GET['id']);

        // если текущий пользователь админ или ответчик коммента
        if (is_admin()) {
            delete_content_date_by_url($content_date['url']);
            delete_comments($content_date['url']);
            delete_rating_by_url($content_date['url']);
        }

        if (empty($_SERVER['HTTP_REFERER'])) {
            redirect_to(ADMIN_CONTENT_DATE_URL);
        } else {
            redirect_to($_SERVER['HTTP_REFERER']);
        }

        die();
    }

    $caption            = 'Рейтинг';
    $content_template   = 'rating';
    $new_comments_count = get_new_comments_count();
    $sort               = null;
    $rating_list        = null;

    if (array_key_exists('sort', $_GET)) {
        $sort = $_GET['sort'];
    }

    if (is_null($sort) || !in_array($sort, array('rating_asc', 'rating_desc', 'count'))) {
        $sort = 'rating_desc';
    }

    $rating_list = get_content_rating_list($sort);

    include(ADMIN_TEMPLATES_DIR.'/layout.tpl');

?>