<?php if (!defined('CONFIG')) die('Hacking attempt!');

	/*
		Paths
	*/
	define('ADMIN_TEMPLATES_DIR', ADMIN_DIR.'/templates');
	define('ADMIN_UPDATE_CACHE_DIR', CACHE_DIR.'/update');
	/*
		URLs
	*/
	define('ADMIN_HOME', SITE_URL.'/system/admin');
	define('ADMIN_HOME_URL', ADMIN_HOME.'/index.php');
	define('ADMIN_MODERS_URL', ADMIN_HOME.'/moders.php');
	define('ADMIN_MODERS_AVATAR_URL', '/include/img/avatar');
	define('ADMIN_MODERS_DELETE_URL', ADMIN_MODERS_URL.'?moder_id=%d&delete');
	define('ADMIN_CONTENT_URL', ADMIN_HOME.'/content.php');
	define('ADMIN_CONTENT_ITEM_URL', ADMIN_CONTENT_URL.'?id=%d');
	define('ADMIN_CONTENT_ITEM_DELETE_URL', ADMIN_CONTENT_ITEM_URL.'&delete');
	define('ADMIN_CONTENT_COMMENT_MODERATE_URL', ADMIN_CONTENT_URL.'?comment_id=%d');
	define('ADMIN_CONTENT_COMMENT_MODERATE_WITH_CONTENT_URL', ADMIN_CONTENT_COMMENT_MODERATE_URL.'&content_id=%d');
	define('ADMIN_CONTENT_COMMENT_DELETE_URL', ADMIN_CONTENT_COMMENT_MODERATE_URL.'&delete');
	define('ADMIN_CONTENT_COMMENT_DELETE_WITH_CONTENT_URL', ADMIN_CONTENT_COMMENT_DELETE_URL.'&content_id=%d');
	define('ADMIN_MODERATE_URL', ADMIN_HOME.'/moderate.php');
	define('ADMIN_MODERATE_COMMENTS_ENABLE', ADMIN_MODERATE_URL.'?comments_enable=1');
	define('ADMIN_MODERATE_COMMENTS_DISABLE', ADMIN_MODERATE_URL.'?comments_enable=0');
	define('ADMIN_TEMPLATES_URL', ADMIN_HOME.'/templates.php');
	define('ADMIN_STAT_URL', ADMIN_HOME.'/stat.php');
	define('ADMIN_STAT_COMMENTS_URL', ADMIN_STAT_URL.'?date=%s');
	define('ADMIN_RATING_URL', ADMIN_HOME.'/rating.php');
	define('ADMIN_RATING_DELETE_URL', ADMIN_RATING_URL.'?id=%s&delete');
	define('ADMIN_RATING_ASC_URL', ADMIN_RATING_URL.'?sort=rating_asc');
	define('ADMIN_RATING_COUNT_URL', ADMIN_RATING_URL.'?sort=count');
	define('ADMIN_CONTENT_DATE_URL', ADMIN_HOME.'/content_date.php');
	define('ADMIN_CONTENT_DATE_EDIT_URL', ADMIN_CONTENT_DATE_URL.'?id=%d');
	define('ADMIN_CONTENT_DATE_DELETE_URL', ADMIN_CONTENT_DATE_EDIT_URL.'&delete');
	define('ADMIN_CONTENT_DATE_SORT_ASC_URL', ADMIN_CONTENT_DATE_URL.'?sort=asc');
	define('ADMIN_CACHE_URL', ADMIN_HOME.'/cache.php');
	define('ADMIN_CACHE_UPDATED_URL', ADMIN_CACHE_URL.'?update');
	define('ADMIN_QUIT_URL', ADMIN_HOME.'/quit.php');
	define('ADMIN_LOGIN_URL', ADMIN_HOME.'/login.php');
	define('ADMIN_NO_PERMISSIONS', ADMIN_HOME.'/no_permissions.php');
	define('ADMIN_UPDATE_URL', ADMIN_HOME.'/update.php');
	define('ADMIN_UPDATE_RUN_URL', ADMIN_UPDATE_URL.'?update');

	define('ADMIN_AVATAR_DIR', INCLUDE_DIR.'/img/avatar');

	define('ADMIN_DEFAULT_ADMIN_PAGE', ADMIN_STAT_URL);
	define('ADMIN_DEFAULT_MODER_PAGE', ADMIN_CONTENT_URL);
	/*
		Misc
	*/
	define('ADMIN_PANEL_NAME', 'Административная панель');
	define('ADMIN_MODER_IMG_NAME', 'img');
	define('ADMIN_MODER_DEFAULT_IMG', ADMIN_HOME.'/img/anon.jpg');
	define('ADMIN_MODER_IMG_WIDTH', 50);
	define('ADMIN_MODER_IMG_HEIGHT', 50);
	define('ADMIN_UPDATE_ARCH', 'https://bitbucket.org/KillBrain/lawsitecore/get/master.zip');

?>