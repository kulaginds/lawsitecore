<?php

	require_once(__DIR__.'/../../include/config.php');
	require_once(SYSTEM_DIR.'/helpers.php');
	require_once(SYSTEM_DIR.'/auth/functions.php');
    require_once(SYSTEM_DIR.'/comments/functions.php');
    require_once(ADMIN_DIR.'/lib/constants.php');
    require_once(ADMIN_DIR.'/lib/functions.php');

    check_auth(ADMIN_LOGIN_URL);
    check_admin(ADMIN_NO_PERMISSIONS);

    $caption            = 'Обновление сайта';
    $content_template   = 'update';
    $new_comments_count = get_new_comments_count();
    $updated            = array_key_exists('update', $_GET);
    $log                = '';

    if ($updated) {
        $log = update_core();
    }

    include(ADMIN_TEMPLATES_DIR.'/layout.tpl');

?>