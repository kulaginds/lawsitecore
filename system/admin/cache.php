<?php

	require_once(__DIR__.'/../../include/config.php');
	require_once(SYSTEM_DIR.'/helpers.php');
	require_once(SYSTEM_DIR.'/auth/functions.php');
    require_once(SYSTEM_DIR.'/comments/functions.php');
    require_once(SYSTEM_DIR.'/cron/functions.php');
    require_once(ADMIN_DIR.'/lib/constants.php');

    check_auth(ADMIN_LOGIN_URL);
    check_admin(ADMIN_NO_PERMISSIONS);

    $caption            = 'Обновление кэша';
    $content_template   = 'cache';
    $new_comments_count = get_new_comments_count();
    $updated            = array_key_exists('update', $_GET);

    if ($updated) {
        update_comments_cache();
    }

    include(ADMIN_TEMPLATES_DIR.'/layout.tpl');

?>