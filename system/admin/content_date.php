<?php

	require_once(__DIR__.'/../../include/config.php');
    require_once(SYSTEM_DIR.'/functions.php');
	require_once(SYSTEM_DIR.'/helpers.php');
	require_once(SYSTEM_DIR.'/auth/functions.php');
    require_once(SYSTEM_DIR.'/comments/functions.php');
    require_once(SYSTEM_DIR.'/rating/functions.php');
    require_once(SYSTEM_DIR.'/date/functions.php');
    require_once(ADMIN_DIR.'/lib/functions.php');
    require_once(ADMIN_DIR.'/lib/helpers.php');
    require_once(ADMIN_DIR.'/lib/constants.php');

    check_auth(ADMIN_LOGIN_URL);
    check_admin(ADMIN_NO_PERMISSIONS);

    if (array_key_exists('date', $_POST)) {
        $content_id = (int)$_POST['content_id'];
        $date       = $_POST['date'];
        
        if (false !== strtotime($date)) {
            update_content_date($content_id, $date);
        }

        redirect_to(ADMIN_CONTENT_DATE_URL);
        die();
    }

    if (array_key_exists('id', $_GET) && array_key_exists('delete', $_GET)) {
        // удалить рейтинг, комменты, дату и сделать редирект на страницу дат статей
        $content_date = get_content_date_by_id((int)$_GET['id']);

        // если текущий пользователь админ или ответчик коммента
        if (is_admin()) {
            delete_content_date_by_url($content_date['url']);
            delete_comments($content_date['url']);
            delete_rating_by_url($content_date['url']);
        }

        if (empty($_SERVER['HTTP_REFERER'])) {
            redirect_to(ADMIN_CONTENT_DATE_URL);
        } else {
            redirect_to($_SERVER['HTTP_REFERER']);
        }

        die();
    }

    $caption            = 'Дата пересмотра статей';
    $content_template   = 'content_date_list';
    $new_comments_count = get_new_comments_count();
    $content_list       = null;

    if (array_key_exists('id', $_GET)) {
        $content_template = 'content_date_form';
        $content_id       = (int)$_GET['id'];
        $content          = get_content_by_id($content_id);
        $date             = null;
        $datepicker       = true;

        if (is_null($content)) {
            redirect_to(ADMIN_CONTENT_DATE_URL);
            die();
        } else {
            $date = $content['date'];
        }
    } else {
        $sort = null;

        if (array_key_exists('sort', $_GET) && $_GET['sort'] == 'asc') {
            $sort = 'asc';
        } else {
            $sort = 'desc';
        }

        $content_list = get_content_list_sort($sort);
    }

    include(ADMIN_TEMPLATES_DIR.'/layout.tpl');

?>