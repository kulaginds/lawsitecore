#!/usr/bin/php
<?php
	
	require_once(__DIR__.'/../include/config.php');
	require_once(SYSTEM_DIR.'/cron/functions.php');

	set_time_limit(0);
	ini_set('memory_limit', -1);

	handle_cron();