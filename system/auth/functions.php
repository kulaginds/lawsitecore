<?php if (!defined('CONFIG')) die('Hacking attempt!');

	require_once(__DIR__.'/constants.php');
	require_once(SYSTEM_DIR.'/functions.php');
	require_once(SYSTEM_DIR.'/db_functions.php');

	session_start();

	/*
		Return true if user authorised
	*/
	function is_auth() {
		return array_key_exists('key', $_SESSION);
	}

	/*
		Has user admin permissions
	*/
	function is_admin() {
		if (is_auth()) {
			return (PERMISSION_ADMIN === $_SESSION['permissions']);
		} else {
			return false;
		}
	}

	/*
		Check is auth user, is no do redirect to $login_page
	*/
	function check_auth($login_page) {
		if (!is_auth()) {
			$_SESSION['redirect_after_login'] = get_page_address();
			redirect_to($login_page);
			die();
		}
	}

	/*
		Check is admin, is no do redirect to $page
	*/
	function check_admin($page) {
		if (!is_admin()) {
			redirect_to($page);
		}
	}

	/*
		To auth user
	*/
	function login_user($password, $page) {
		$password = add_salt_password($password);

		if (ADMIN_PASSWORD == $password) {
			$_SESSION['key'] = $password;
			$_SESSION['permissions'] = PERMISSION_ADMIN;
		} else if (false !== ($moder = _check_moder($password))) {
			$_SESSION['key'] = $password;
			$_SESSION['permissions'] = PERMISSION_MODER;
			$_SESSION['moder'] = $moder;
		} else {
			return false;
		}
		
		if (!empty($_SESSION['redirect_after_login'])) {
			$page = $_SESSION['redirect_after_login'];
			unset($_SESSION['redirect_after_login']);
		}

		redirect_to($page);
		die();
	}

	/*
		Check moder in database. Return his name.
	*/
	function _check_moder($password) {
		global $link;

		$sql = 'SELECT id, name, img FROM `moders` WHERE `pass` = ?';
		$stmt = $link->prepare($sql);
		$stmt->bind_param('s', $password);
		$stmt->execute();

		$result = $stmt->get_result();
		$moder   = null;

		if ($result->num_rows > 0) {
			$moder = $result->fetch_assoc();
		}

		unset($sql, $stmt, $result);

		return empty($moder) ? false : $moder;
	}

	/*
		Log out user
	*/
	function logout_user($page) {
		if (is_auth()) {
			session_destroy();
		}

		redirect_to($page);
	}

	/*
		Add salt to password
	*/
	function add_salt_password($password) {
		return md5(SALT.$password);
	}

	/*
		Return moder data
	*/
	function get_moder() {
		return (is_auth() && !is_admin()) ? $_SESSION['moder'] : null;
	}

?>