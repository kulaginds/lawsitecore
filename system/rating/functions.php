<?php if (!defined('CONFIG')) die('Hacking attempt!');
	
	require_once(SYSTEM_DIR.'/db_functions.php');

	/*
		Return rating value and count of voters
	*/
	function get_rating($content_url) {
		global $link;

		$sql  = '(SELECT COUNT(value) AS count FROM rating_votes WHERE url=? AND value=\'plus\')
				UNION ALL
				(SELECT COUNT(value) AS count FROM rating_votes WHERE url=? AND value=\'minus\')';
		$stmt = $link->prepare($sql);
		$stmt->bind_param("ss", $content_url, $content_url);
		$stmt->execute();

		$result      = $stmt->get_result();
		$count_plus  = $result->fetch_object()->count;
		$count_minus = $result->fetch_object()->count;

		unset($sql, $stmt, $result);

		$count  = $count_plus + $count_minus;
		$rating = 0;
		
		if ($count > 0) {
			$rating = ($count_plus * 10 + $count_minus * 5) / $count;
			$rating = round($rating, 1);
		}

		return array($rating, $count);
	}

	/*
		Vote to page
	*/
	function vote_page($content_url, $value) {
		global $link;

		$sql = 'INSERT INTO rating_votes(url, value) VALUES(?, ?)';
		$stmt = $link->prepare($sql);
		$stmt->bind_param("ss", $content_url, $value);
		$stmt->execute();

		$affected_rows = $stmt->affected_rows;

		unset($sql, $stmt);

		return ($affected_rows > 0);
	}

	/*
		Return rating stat
	*/
	function get_content_rating_list($sort) {
		global $link;

		$sql      = 'SELECT id AS content_id, url FROM `content_date`';
		$result   = $link->query($sql);
		$response = array();

		if ($result->num_rows > 0) {
			$response = $result->fetch_all(MYSQLI_ASSOC);
		} else {
			return $response;
		}

		for ($i = 0; $i < count($response); $i++) {
			$rating                 = get_rating($response[$i]['url']);
			$response[$i]['rating'] = $rating[0];
			$response[$i]['count']  = $rating[1];
		}

		switch ($sort) {
			case 'rating_asc':
				uasort($response, '_sort_by_rating_asc');
				break;
			case 'count':
				uasort($response, '_sort_by_count_desc');
				break;
			default:
				uasort($response, '_sort_by_rating_desc');
				break;
		}

		return $response;
	}

	/*
		Remove rating by content url
	*/
	function delete_rating_by_url($url) {
		global $link;

		$sql  = 'DELETE FROM rating_votes WHERE url=?';

		$stmt = $link->prepare($sql);
		$stmt->bind_param('s', $url);
		$stmt->execute();

		$affected_rows = $stmt->affected_rows;

		unset($sql, $stmt);

		return ($affected_rows > 0);
	}

	/*
		Sort by rating asc
	*/
	function _sort_by_rating_asc($a, $b) {
		if ($a['rating'] == $b['rating']) {
			return 0;
		}

		return ($a['rating'] < $b['rating']) ? -1 : 1;
	}

	/*
		Sort by rating desc
	*/
	function _sort_by_rating_desc($a, $b) {
		if ($a['rating'] == $b['rating']) {
			return 0;
		}

		return ($a['rating'] < $b['rating']) ? 1 : -1;
	}

	/*
		Sort by count desc
	*/
	function _sort_by_count_desc($a, $b) {
		if ($a['count'] == $b['count']) {
			return 0;
		}

		return ($a['count'] < $b['count']) ? 1 : -1;
	}

?>