<?php if (!defined('CONFIG')) die('Hacking attempt!');


    require_once(SYSTEM_DIR.'/functions.php');
    require_once(SYSTEM_DIR.'/helpers.php');
    require_once(__DIR__.'/functions.php');

    $page_address = get_page_address();
    $name         = '';
    $text         = '';
    $response     = array(
        'status'  => null,
        'errors'  => array(array(), null),
        'message' => null,
    );

    if(array_key_exists('addmessage', $_POST) && $_POST['addmessage'] == 1) {
        $response = handle_comment();
    }

    $comments_cache = get_comments_cache($page_address);

    if (!empty($comments_cache)) {
        print $comments_cache;

        if(!has_comments_lock()) {
            include(__DIR__.'/comments_form.tpl');
        }
    }

?>