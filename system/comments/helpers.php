<?php if (!defined('CONFIG')) die('Hacking attempt!');

	/*
		Render comments
	*/
	function render_comments($comments, $content_id = null) {
		$html = '';

		if (count($comments) > 0) {
			foreach ($comments as $item) {
				$html .= '<table class="srok_naloga">'.
                                '<tbody>'.
                                    '<tr>'.
	                                    '<td class="srok_naloga3"><a href="'.$item['url'].'" target="_blank">'.$item['url'].'</a></td>'.
	                                    '<td class="srok_naloga1">' . date('d.m.Y', strtotime($item['date'])) . '</td>';

	            if (is_null($content_id)) {
	                $html .= '<td class="srok_naloga2"><a href="' . sprintf(ADMIN_CONTENT_COMMENT_MODERATE_URL, $item['id']) . '">' . mb_substr(strip_tags($item["text"]), 0, 60) . '..</a> </td>';
	            } else {
	            	$html .= '<td class="srok_naloga2"><a href="' . sprintf(ADMIN_CONTENT_COMMENT_MODERATE_WITH_CONTENT_URL, $item['id'], $content_id) . '">' . mb_substr(strip_tags($item["text"]), 0, 60) . '..</a> </td>';
	            }

	            $html .= '<td class="srok_naloga3">' . render_comment_status($item['date'], $item['moder_id']) . '</td>';

	            if (is_null($content_id)) {
	            	$html .= '<td class="srok_naloga4">[<a href="' . sprintf(ADMIN_CONTENT_COMMENT_DELETE_URL, $item['id']) . '" style="color: #f00;" onClick="return confirm(\'Вы действительно хотите удалить эту запись?\');">удалить</a>]</td>';
	           	} else {
	           		$html .= '<td class="srok_naloga4">[<a href="' . sprintf(ADMIN_CONTENT_COMMENT_DELETE_WITH_CONTENT_URL, $item['id'], $content_id) . '" style="color: #f00;" onClick="return confirm(\'Вы действительно хотите удалить эту запись?\');">удалить</a>]</td>';
	           	}

                $html .=            '</tr>'.
                                '</tbody>'.
                            '</table>';
			}
		} else {
			$html = '<h1>Нет новых комментариев</h1>';
		}

		return $html;
	}

	/*
		Render comment status
	*/
	function render_comment_status($comment_date, $moder_id) {
		if ($comment_date <= date("Y-m-d")) {
            $html = '<font style="color: #090;">Опубликовано</font>';
        } else {
            $html = '<font style="color: #f00;">Не опубликовано</font>';  
        }

        if ($moder_id == -777) {
            $html = '<font style="color: brown;">На модерации</font>';
        }

        return $html;
	}

?>