<a name="addcomment"></a>
<div class="comment-form" id="comment_form">
    <form method="POST" id="addcommentform" action="#addcomment">
        <input type="hidden" name="addmessage" value="1"/>
        <input type="hidden" name="comment_page" value="<?=$page_address;?>">
      <div class="form-heading">Добавить комментарий</div>
      <div class="form">
        <div class="form-group<?=print_text_param(array($response['errors'][0], 'comment_name'), ' error');?>" id="comment-name-group">
          <label for="comment-name" class="form-label">Ваше имя:</label>
          <input type="text" id="comment-name" name="comment_name" class="form-control" value="<?=print_param($name);?>">
        </div>
        <div class="form-group<?=print_text_param(array($response['errors'][0], 'comment_text'), ' error');?>" id="comment-text-group">
          <label for="comment-text" class="form-label">Комментарий</label>
          <textarea id="comment-text" name="comment_text" class="form-control form-control_textarea"><?=print_param($text);?></textarea>
        </div>
        <div class="recaptcha">
          <div class="g-recaptcha" data-sitekey="<?=CAPTCHA_PUBLIC_KEY;?>"></div>
        </div>
        <input type="submit" class="btn btn_form" value="Добавить комментарий">
      </div>
    <? if ($response['status'] == STATUS_ERRORS): ?>
      <div class="message error"><?=$response['errors'][1];?></div>
    <? elseif ($response['status'] == STATUS_OK): ?>
      <div class="message success"><?=$response['message'];?></div>
    <? endif; ?>
    </form>
    </div>
    <script src='https://www.google.com/recaptcha/api.js?hl=ru'></script>
