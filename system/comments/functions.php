<?php if (!defined('CONFIG')) die('Hacking attempt!');

	require_once(SYSTEM_DIR.'/functions.php');
	require_once(SYSTEM_DIR.'/db_functions.php');
	require_once(SYSTEM_DIR.'/moders/functions.php');
	require_once(__DIR__.'/constants.php');
	require_once(__DIR__.'/messages.php');

	/*
		Handle comment
	*/
	function handle_comment() {
		global $messages, $link;

        if(has_comments_lock()) {
        	return array(array(), $messages['comments_locked']);
        }

		$name    = $_POST['comment_name'];
		$text    = $_POST['comment_text'];
		$page    = $_POST['comment_page'];
		$captcha = $_POST['g-recaptcha-response'];

		$errors  = _check_comment_errors($name, $text, $page, $captcha);
		$message = null;

	    if(true === $errors) {
	        if(add_comment($page, $name, $text)) {
                $message = $messages['success_message'];
                $errors = array(array(), null);
            } else {
                $errors = array(array(), $messages['unknown_error']);
            }
	    }

	    return array(
				'status'  => empty($message) ? STATUS_ERRORS : STATUS_OK,
				'errors'  => $errors,
				'message' => $message,
	        );
	}

	/*
		Check comment errors
	*/
	function _check_comment_errors($name, $text, $page, $captcha) {
		global $messages;

	    $errors = array();

	    if(empty($name) || empty($text)) {
	        $errormessage = $messages['type_all_fields'];

	        if(empty($name)) {
		        $errors['comment_name'] = 1;
		    }
		    if(!$text) {
		        $errors['comment_text'] = 1;
		    }
	    }
	    
	    if(!empty($errors)) {
	        return array($errors, $errormessage);
	    }

	    if(preg_match('#\[b\](.+)\[\/b\]#iUs', $name)
	    	|| preg_match('#<[^>]+>#iUs', $name)) {
	        $errors['comment_name'] = 1;
	        $errormessage = $messages['html_and_bb_forbidden'];
	    }

	    if(preg_match('#\[b\](.+)\[\/b\]#iUs', $text)
	    	|| preg_match('#<[^>]+>#iUs', $text)) {
	        $errors['comment_text'] = 1;
	        $errormessage = $messages['html_and_bb_forbidden'];
	    }

	    if($errors) {
	        return array($errors, $errormessage);
	    }

	    if (!is_file(SITE_DIR.$page)) {
           return array(array(), $messages['wrong_comment_page']);
        }

	    if(!_check_comment_captcha($_POST['g-recaptcha-response'])) {
	        return array(array(), $messages['captcha_invalid']);
	    }

	    return true;
	}

	/*
		Check comment captcha
	*/
	function _check_comment_captcha($recaptchaResponse) {
		if (DEBUG) {
			return true;
		}

	    if(empty($recaptchaResponse)){
	        return false;
	    }

	    $data = http_build_query(
        	array(
				'secret'   =>CAPTCHA_PRIVATE_KEY,
				'response' =>$recaptchaResponse,
				'remoteip' =>$_SERVER['REMOTE_ADDR']
            )
        );
        $context = stream_context_create(
        	array(
	            'http' => array(
					'method'  => 'POST',
					'header'  => "Content-type: application/x-www-form-urlencoded\r\n"."Content-Length: " . strlen($data) . "\r\n",
					'content' => $data
	            )
            )
        );
        $googleResponse = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context));

        return (true === $googleResponse->success);
	}

	/*
		Add comment
	*/
	function add_comment($page, $name, $text, $date = null) {
		global $link;

		if (empty($date)) {
			$date = date('Y-m-d');
		}

		$img = get_random_avatar();
        $sql = "INSERT INTO comments(url, img, name, text, date)
        		VALUES(?, ?, ?, ?, ?)";
        $stmt = $link->prepare($sql);
        $stmt->bind_param('sssss', $page, $img, $name, $text, $date);
        $stmt->execute();

        $affected_rows = $stmt->affected_rows;

        unset($sql, $stmt);

        return ($affected_rows > 0);
	}

	/*
		Has comment lock
	*/
	function has_comments_lock() {
		return file_exists(INCLUDE_DIR.'/comments.lock');
	}

	/*
		Lock comments
	*/
	function lock_comments() {
		return (bool)file_put_contents(INCLUDE_DIR.'/comments.lock', '1');
	}

	/*
		Unlock comments
	*/
	function unlock_comments() {
		return unlink(INCLUDE_DIR.'/comments.lock');
	}

	/*
		Return page comments cache
	*/
	function get_comments_cache($content_url) {
		$cache_url = COMMENTS_CACHE_DIR.'/'.md5($content_url);

		return is_file($cache_url) ? file_get_contents($cache_url) : '';
	}

	/*
		Add fake comment
	*/
	function add_fake_comment($content_url, $name, $img, $text, $moder_id, $answer, $date = null) {
		global $link;

		if (is_null($date)) {
			$date = date('Y-m-d');
		}

		$sql  = 'INSERT INTO `comments`(url, name, img, text, moder_id, answer, date) 
            	VALUES(?, ?, ?, ?, ?, ?, ?)';
        $stmt = $link->prepare($sql);
        $stmt->bind_param('ssssiss', $content_url, $name, $img, $text, $moder_id, $answer, $date);
        $stmt->execute();

        if ($stmt->affected_rows == 0) {
        	return false;
        }

        update_moder_comments($moder_id, 1);
        unset($sql, $stmt);

        return true;
	}

	/*
		Update comment
	*/
	function update_comment($comment_id, $name, $img, $text, $moder_id, $answer, $date = null) {
		global $link;

		if (is_null($date)) {
			$date = date('Y-m-d');
		}

		$comment = get_comment_by_id($comment_id);
		$sql     = 'UPDATE `comments` SET `name` = ?, `img` = ?, `text` = ?, `date` = ?, `moder_id` = ?, `answer` = ? WHERE `id` = ?';
		$stmt    = $link->prepare($sql);

		$stmt->bind_param('ssssisi', $name, $img, $text, $date, $moder_id, $answer, $comment_id);
		$stmt->execute();

		$affected_rows = $stmt->affected_rows;

		unset($sql, $stmt);

		if ($comment['moder_id'] == -777) {
			update_moder_comments($moder_id, 1);
		}

		return $affected_rows;
	}

	/*
		Return count of new comments
	*/
	function get_new_comments_count() {
		global $link;

		$sql   = 'SELECT COUNT(*) AS count FROM `comments` WHERE `moder_id`=\'-777\'';
		$count = $link->query($sql)->fetch_object()->count;

		unset($sql);

		return $count;
	}

	/*
		Return array of date and count comments between $date1 and $date2
	*/
	function get_comments_stat_between($date1, $date2) {
		global $link;

		$sql = 'SELECT date, COUNT(*) AS count FROM comments WHERE date BETWEEN ? AND ? GROUP BY date';
		
		// as default $date1 <= $date2
		if (strtotime($date1) > strtotime($date2)) {
			// swap it
			$tmp   = $date1;
			$date1 = $date2;
			$date2 = $tmp;
		}

		$stmt = $link->prepare($sql);
		$stmt->bind_param('ss', $date1, $date2);
		$stmt->execute();

		$result = $stmt->get_result();

		$response = array();

		if ($result->num_rows > 0) {
			$response = $result->fetch_all(MYSQLI_ASSOC);
		}

		unset($sql, $stmt, $result);

		return $response;
	}

	/*
		Return comments by content url
	*/
	function get_comments_by_content_url($content_url, $checked = false) {
		global $link;

		$sql = 'SELECT * FROM `comments` WHERE `url` = ?';

		if ($checked) {
			$sql .= ' AND moder_id != -777 AND date <= NOW()';
		}

		$sql .= ' ORDER BY date DESC';

		$stmt = $link->prepare($sql);
		$stmt->bind_param('s', $content_url);
		$stmt->execute();

		$result   = $stmt->get_result();
		$response = array();

		if ($result->num_rows > 0) {
			$response = $result->fetch_all(MYSQLI_ASSOC);
		}

		unset($sql, $stmt, $result);

		return $response;
	}

	/*
		Return array of comments by date
	*/
	function get_comments_by_date($date) {
		global $link;

		$sql  = 'SELECT * FROM `comments` WHERE `date` = ?';
		$stmt = $link->prepare($sql);
		$stmt->bind_param('s', $date);
		$stmt->execute();

		$result   = $stmt->get_result();
		$response = array();

		if ($result->num_rows > 0) {
			$response = $result->fetch_all(MYSQLI_ASSOC);
		}

		unset($sql, $stmt, $result);

		return $response;
	}

	/*
		Return array of unchecked comments
	*/
	function get_unchecked_comments() {
		global $link;

		$sql      = 'SELECT * FROM `comments` WHERE `moder_id` = -777 ORDER BY `id` DESC';
		$result   = $link->query($sql);
		$response = array();

		if ($result->num_rows > 0) {
			$response = $result->fetch_all(MYSQLI_ASSOC);
		}

		unset($sql, $result);

		return $response;
	}

	/*
		Remove comment
	*/
	function delete_comment($comment_id) {
		global $link;

		$sql  = 'DELETE FROM `comments` WHERE `id` = ?';
		$stmt = $link->prepare($sql);
		$stmt->bind_param('i', $comment_id);
		$stmt->execute();

		$affected_rows = $stmt->affected_rows;

		unset($sql, $stmt);

		return ($affected_rows > 0);
	}

	/*
		Remove comments by content url
	*/
	function delete_comments($url) {
		global $link;

		$sql  = 'DELETE FROM comments WHERE url=?';

		$stmt = $link->prepare($sql);
		$stmt->bind_param('s', $url);
		$stmt->execute();

		$affected_rows = $stmt->affected_rows;

		unset($sql, $stmt);

		return ($affected_rows > 0);
	}

	/*
		Return comment by comment id
	*/
	function get_comment_by_id($comment_id) {
		global $link;

		$sql = 'SELECT * FROM comments WHERE id = ?';
		$stmt = $link->prepare($sql);

		$stmt->bind_param('i', $comment_id);
		$stmt->execute();

		$result   = $stmt->get_result();
		$response = null;

		unset($sql, $stmt);

		if ($result->num_rows > 0) {
			$response = $result->fetch_assoc();
		}

		return $response;
	}

	/*
		Update comments cache
	*/
	function update_comments_cache() {
		$pages    = _get_content_list();
		$moders   = get_moders();
		$template = array(
			'answer'  => get_template_code('answer'),
			'block'   => get_template_code('block'),
			'comment' => get_template_code('comment'),
		);

		if (!is_dir(COMMENTS_CACHE_DIR)) {
			mkdir(COMMENTS_CACHE_DIR, 0777, true);
		}

		foreach ($pages as $page) {
			$cache_url = COMMENTS_CACHE_DIR.'/'.md5($page['url']);
			$comments  = get_comments_by_content_url($page['url'], true);
			$fout      = '';
			$result    = $fout;

			if (count($comments) > 0) {
				foreach ($comments as $comment) {
					$c1 = $template["comment"];
	                $c1 = str_replace("\$img", $comment["img"], $c1);
	                $c1 = str_replace("\$name", $comment["name"], $c1);
	                $c1 = str_replace("\$text", str_replace("\n", "<br/>", $comment["text"]), $c1);
	                $c1 = str_replace("\$date", date('d.m.Y', strtotime($comment["date"])), $c1);
	                //// добавляем в код кэша
	                $fout .= $c1;
	                //// если есть ответ модератора и он проверен ////
	                if(!empty($comment["answer"]) && $comment["moder_id"] != -777){
	                    $c2 = $template["answer"];
	                    //// грузим инфу о модераторе ////
	                    $key = array_search($comment["moder_id"], array_column($moders, 'id'));

	                    if (false === $key) {
	                    	// если модератор не найден
	                    	continue;
	                    }

	                    $moder = $moders[$key];
	                    
	                    //// рендерим html-код ответа ////
	                    $c2 = str_replace("\$moder_img", $moder["img"], $c2);
	                    $c2 = str_replace("\$moder_name", $moder["name"], $c2);
	                    $c2 = str_replace("\$name", $comment["name"], $c2);
	                    $c2 = str_replace("\$moder_comments", $moder["comments"], $c2);
	                    $c2 = str_replace("\$answer", str_replace("\n", "<br/>", $comment["answer"]), $c2);

	                    //// добавляем в код кэша
	                    $fout .= $c2;
	                }
				}

				//// рендерим html-код глобальных переменных шаблона ////
	            $result = str_replace("\$comments_count", count($comments),  $template["block"]);
	            $result = str_replace("\$comments", $fout, $result);
			}

            $fp = fopen($cache_url, 'w+');
            if (false === fwrite($fp, $result)) {
            	print "can't save cache for page: ".$page['url']."\n";
            }
            fclose($fp);
		}
	}

	/*
		Return content pages list
		specially for update_comments_cache()
	*/
	function _get_content_list() {
		global $link;

		$sql      = 'SELECT * FROM content_date ORDER BY url ASC';
		$result   = $link->query($sql);
		$response = array();

		if ($result->num_rows > 0) {
			$response = $result->fetch_all(MYSQLI_ASSOC);
		}

		unset($sql, $result);

		return $response;
	}

?>