$(document).ready(function() {
  $("#addcommentform").submit(function(e) {
    e.preventDefault(); // выключаем перезагрузку страницы
    var comment_name = $(this).find('input[name="comment_name"]'),
        comment_text = $(this).find('textarea[name="comment_text"]'),
        params = {
      'ajax':1,
      'addmessage':1,
      'comment_name':comment_name.val(),
      'comment_text':comment_text.val(),
      'comment_page':$(this).find('input[name="comment_page"]').val(),
      'g-recaptcha-response':$(this).find('textarea[name="g-recaptcha-response"]').val()
    };
    $.post('/system/comments/ajax.php', params, function(response) {
      $('#comment-name-group').removeClass('error');
      $('#comment-text-group').removeClass('error');
      $('#comment_form .message').remove();

      if (response.status == 'ok') {
        // отображаем сообщение об успехе
        $("#addcommentform")
          .append(
            $('<div />', { class:'message success' })
              .html(response.message)
          )
          .delay(5000)
          .queue(function() {
              comment_name.val('');
              comment_text.val('');
              $('#addcommentform .message.success').fadeOut();
              $(this).dequeue();
            }
          );
      } else {
        // устанавливаем поле с ошибкой красного цвета и отображаем сообщение об ошибке
        if (response.errors[0].comment_name) {
          $('#comment-name-group').addClass('error');
        }

        if (response.errors[0].comment_text) {
          $('#comment-text-group').addClass('error');
        }

        $("#addcommentform").append($('<div />', { class:'message error' }).html(response.errors[1]));
      }
    });
  });
});