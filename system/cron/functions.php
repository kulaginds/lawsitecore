<?php if (!defined('CONFIG')) die('Hacking attempt!');

	require_once(SYSTEM_DIR.'/db_functions.php');
	require_once(SYSTEM_DIR.'/comments/functions.php');
	
	/*
		Cron handler
	*/
	function handle_cron() {
		update_comments_cache();
	}

?>