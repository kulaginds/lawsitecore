<?php if (!defined('CONFIG')) die('Hacking attempt!');

	require_once(SYSTEM_DIR.'/db_functions.php');

	/*
		Return array of moders
	*/
	function get_moders() {
		global $link;

		$sql      = 'SELECT id,name,img,comments FROM `moders`';
		$result   = $link->query($sql);
		$response = array();

		if ($result->num_rows > 0) {
			$response = $result->fetch_all(MYSQLI_ASSOC);
		}

		return $response;
	}

	/*
		Delete moder
	*/
	function delete_moder($moder_id) {
		global $link;

		$link->autocommit(false);

		$sql  = 'DELETE FROM `moders` WHERE `id` = ?';
		$stmt = $link->prepare($sql);
        $stmt->bind_param('i', $moder_id);
        $stmt->execute();

		$sql  = 'UPDATE `comments` SET `moder_id` = -777, `answer` = NULL WHERE `moder_id` = ?';
		$stmt = $link->prepare($sql);
        $stmt->bind_param('i', $moder_id);
        $stmt->execute();

        if (!$link->commit()) {
        	unset($sql, $stmt);
        	return false;
        }

        $link->autocommit(true);
        unset($sql, $stmt);

        return true;
	}

	/*
		Add moder
	*/
	function add_moder($name, $pass, $img) {
		global $link;

		$sql = 'INSERT INTO `moders`(name, pass, img) VALUES(?, ?, ?)';
		$stmt = $link->prepare($sql);
		$stmt->bind_param('sss', $name, $pass, $img);
		$stmt->execute();

		return ($stmt->affected_rows > 0);
	}

	/*
		Update moder comments count
	*/
	function update_moder_comments($moder_id, $count) {
		global $link;

		$sql = 'UPDATE `moders` SET `comments` = comments + ? WHERE `id` = ?';
        $stmt = $link->prepare($sql);
        $stmt->bind_param('ii', $count, $moder_id);
        $stmt->execute();

        unset($sql, $stmt);
	}

?>