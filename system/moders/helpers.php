<?php if (!defined('CONFIG')) die('Hacking attempt!');

	/*
		Render moders select box
	*/
	function render_moders_box($moders, $default_moder_id = null) {
		$html = '';

		if (count($moders) > 0) {
			$html = '<select name="moder_id" style="width: 300px;">';

        	foreach ($moders as $moder) {
        		if (!is_null($default_moder_id) && $default_moder_id == $moder['id']) {
        			$html .= '<option value="' . $moder['id'] . '" selected>' . $moder['name'] . '</option>';
        		} else {
        			$html .= '<option value="' . $moder['id'] . '">' . $moder['name'] . '</option>';
        		}
        	}
        	
        	$html .= '</select>';
        } else {
        	$html = '<p>нет модераторов</p>';
        }

        return $html;
	}

?>