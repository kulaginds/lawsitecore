<?php if (!defined('CONFIG')) die('Hacking attempt!');

	/*
		Return page address
	*/
	function get_page_address() {
		return str_replace($_SERVER['DOCUMENT_ROOT'], '', $_SERVER['SCRIPT_FILENAME']);
	}

	/*
		Do redirect to $page
	*/
	function redirect_to($page) {
		header("Location: $page");
	}

	/*
		Getting file from remote server. Return file addr
	*/
	function load_file($url, $storage_dir, $allowed_types) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		
		if (false === $data) {
			return null;
		}

		$contenttype = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);

		curl_close($ch);

		$fileext     = null;
		$filetype    = null;
		
		foreach ($allowed_types as $type => $ext) {
			if (false !== mb_strpos($contenttype, $type)) {
				$fileext = $ext;
				$filetype = $type;
				break;
			}
		}

		if (is_null($fileext)) {
			return null;
		}

		$destination = random_filename($storage_dir, $fileext);
		$file        = fopen($destination, "w+");
		fputs($file, $data);
		fclose($file);

		return array(
			'name' => $destination,
			'ext'  => $fileext,
			'type' => $filetype,
		);
	}

	/*
		Return random name for file
	*/
	function random_filename($dir, $ext) {
		$filename = false;

		while (false === $filename) {
			$addr = $dir.'/'.md5(uniqid()).$ext;

			if (!is_file($addr)) {
				$filename = $addr;
			}
		}

		return $filename;
	}

	/*
		Return random avatar
	*/
	function get_random_avatar() {
		$imgs = array();
		$hdl  = opendir(ADMIN_FAKE_AVATAR_DIR) or die("can't open direct");
		$img  = ANON_DEFAULT_IMG;

        while ($file_name = readdir($hdl)) {
        	$file_addr = ADMIN_FAKE_AVATAR_DIR.'/'.$file_name;

            if (($file_name != ".")
            	&& ($file_name != "..")
            	&& is_file($file_addr)) {
            	$imgs[] = str_replace(SITE_DIR, '', $file_addr);
            }
        }

        if (count($imgs) > 0) {
        	$img = $imgs[rand(0, count($imgs)-1)];
        }

        return $img;
	}

	/*
		Return random name
	*/
	function get_random_name() {
		$file  = file_get_contents(INCLUDE_DIR.'/names.txt');
        $file  = str_replace("\r\n", "\n", $file);
        $names = explode("\n", $file);
        $key   = array_search('', $names);

        if ($key !== false) {
        	unset($names[$key]);
        }

        $name = $names[rand(0, count($names)-1)];

        if(empty($name)) {
            $name = "Михаил";
        }

        return $name;
	}

	/*
		Return template code
	*/
	function get_template_code($name) {
		$filename = TEMPLATES_DIR.'/'.$name.'.tpl';

		if (!is_file($filename)) {
			return '';
		}

		return file_get_contents($filename);
	}

	/*
		Save template code
	*/
	function save_template_code($name, $code) {
		$filename = TEMPLATES_DIR.'/'.$name.'.tpl';

		if (!is_file($filename)) {
			return false;
		}

		return file_put_contents($filename, $code);
	}

	/**
	 * This file is part of the array_column library
	 *
	 * For the full copyright and license information, please view the LICENSE
	 * file that was distributed with this source code.
	 *
	 * @copyright Copyright (c) Ben Ramsey (http://benramsey.com)
	 * @license http://opensource.org/licenses/MIT MIT
	 */
	if (!function_exists('array_column')) {
	    /**
	     * Returns the values from a single column of the input array, identified by
	     * the $columnKey.
	     *
	     * Optionally, you may provide an $indexKey to index the values in the returned
	     * array by the values from the $indexKey column in the input array.
	     *
	     * @param array $input A multi-dimensional array (record set) from which to pull
	     *                     a column of values.
	     * @param mixed $columnKey The column of values to return. This value may be the
	     *                         integer key of the column you wish to retrieve, or it
	     *                         may be the string key name for an associative array.
	     * @param mixed $indexKey (Optional.) The column to use as the index/keys for
	     *                        the returned array. This value may be the integer key
	     *                        of the column, or it may be the string key name.
	     * @return array
	     */
	    function array_column($input = null, $columnKey = null, $indexKey = null)
	    {
	        // Using func_get_args() in order to check for proper number of
	        // parameters and trigger errors exactly as the built-in array_column()
	        // does in PHP 5.5.
	        $argc = func_num_args();
	        $params = func_get_args();
	        if ($argc < 2) {
	            trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
	            return null;
	        }
	        if (!is_array($params[0])) {
	            trigger_error(
	                'array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given',
	                E_USER_WARNING
	            );
	            return null;
	        }
	        if (!is_int($params[1])
	            && !is_float($params[1])
	            && !is_string($params[1])
	            && $params[1] !== null
	            && !(is_object($params[1]) && method_exists($params[1], '__toString'))
	        ) {
	            trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
	            return false;
	        }
	        if (isset($params[2])
	            && !is_int($params[2])
	            && !is_float($params[2])
	            && !is_string($params[2])
	            && !(is_object($params[2]) && method_exists($params[2], '__toString'))
	        ) {
	            trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
	            return false;
	        }
	        $paramsInput = $params[0];
	        $paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;
	        $paramsIndexKey = null;
	        if (isset($params[2])) {
	            if (is_float($params[2]) || is_int($params[2])) {
	                $paramsIndexKey = (int) $params[2];
	            } else {
	                $paramsIndexKey = (string) $params[2];
	            }
	        }
	        $resultArray = array();
	        foreach ($paramsInput as $row) {
	            $key = $value = null;
	            $keySet = $valueSet = false;
	            if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
	                $keySet = true;
	                $key = (string) $row[$paramsIndexKey];
	            }
	            if ($paramsColumnKey === null) {
	                $valueSet = true;
	                $value = $row;
	            } elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
	                $valueSet = true;
	                $value = $row[$paramsColumnKey];
	            }
	            if ($valueSet) {
	                if ($keySet) {
	                    $resultArray[$key] = $value;
	                } else {
	                    $resultArray[] = $value;
	                }
	            }
	        }
	        return $resultArray;
	    }
	}

	/*
		Remove recursive all files in directory
	*/
	function rrmdir($dir, $head = true) {
		if (is_dir($dir)) {
			$objects = scandir($dir);
			
			foreach ($objects as $object) {
				if ($object != "." && $object != "..") {
					if (is_dir($dir."/".$object)) {
						rrmdir($dir."/".$object, false);
					} else {
						unlink($dir."/".$object);
					}
				}
			}

			if (!$head) {
				rmdir($dir);
			}
		}
	}

?>