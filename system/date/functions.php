<?php if (!defined('CONFIG')) die('Hacking attempt!');
	
	require_once(SYSTEM_DIR.'/db_functions.php');

	/*
		Return content date or null
	*/
	function get_content_date($content_url) {
		global $link;

		$sql = 'SELECT date FROM `content_date` WHERE `url` = ? LIMIT 1';
		$stmt = $link->prepare($sql);
		$stmt->bind_param('s', $content_url);
		$stmt->execute();

		$result = $stmt->get_result();
		$date = null;

		if ($result->num_rows > 0) {
			$date = $result->fetch_object()->date;
		}

		unset($sql, $stmt, $result);

		return $date;
	}

	/*
		Put content date into database. Return date.
	*/
	function put_content_date($content_url, $date = null) {
		global $link;

		if (empty($date)) {
			$date = date("Y-m-d");
		}

		$sql  = 'INSERT INTO `content_date`(url, date) VALUES(?, ?)';

		$stmt = $link->prepare($sql);
		$stmt->bind_param('ss', $content_url, $date);
		$stmt->execute();

		unset($sql, $stmt);

		return $date;
	}

	/*
		Return content & date by id
	*/
	function get_content_date_by_id($content_id) {
		global $link;

		$sql    = 'SELECT * FROM content_date WHERE id=?';

		$stmt   = $link->prepare($sql);
		$stmt->bind_param('i', $content_id);
		$stmt->execute();

		$result = $stmt->get_result();

		return ($result->num_rows > 0) ? $result->fetch_assoc() : null;
	}

	/*
		Delete content_date by url
	*/
	function delete_content_date_by_url($url) {
		global $link;

		$sql = 'DELETE FROM content_date WHERE url=?';

		$stmt = $link->prepare($sql);
		$stmt->bind_param('s', $url);
		$stmt->execute();

		return ($stmt->affected_rows > 0);
	}

?>